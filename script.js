let start = document.getElementsByClassName("start")[0];
let clear = document.getElementsByClassName("clear")[0];
let timerWindowMin = document.getElementsByClassName("timer-window-min")[0];
let timerWindowSec = document.getElementsByClassName("timer-window-sec")[0];
let timerWindowMs = document.getElementsByClassName("timer-window-ms")[0];
let timerId;
let startValue = 0;

function timerRun() {
   if (start.value == "Start") {
    start.value = "Pause";
    timerId = setInterval(function() {
      startValue = startValue + 0.006;
      timerWindowMs.value = startValue.toFixed(3).slice(2);
      timerWindowSec.value = Math.trunc(startValue);
      if (startValue >= 60) {
        timerWindowMin.value++;
        startValue = 0;
      }
    }, 6);
  } else {
    start.value = "Start";
    clearInterval(timerId);
  }
}
start.onclick = timerRun;
clear.onclick = function() {
  clearInterval(timerId);
  timerWindowMin.value =timerWindowSec.value =timerWindowMs.value=startValue=0;
    if (start.value == "Pause") {
    start.value = "Start";
  }
};
